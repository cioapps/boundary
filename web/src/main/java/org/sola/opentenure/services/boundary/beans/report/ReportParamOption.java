package org.sola.opentenure.services.boundary.beans.report;

import java.io.Serializable;

/**
 * Report parameter option
 */
public class ReportParamOption implements Serializable {
    private static final long serialVersionUID = 7526472295622776147L;
    
    private String label;
    private boolean selected;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
